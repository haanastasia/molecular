# Molecular #

New project using Bootstrap 4.

## Getting Started ##

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites ###
* node.js or docker

### Installing vie webpack ###
* Download project ( git clone https://haanastasia@bitbucket.org/haanastasia/molecular.git )
* Run node.js
* npm run build
* npm run start

### Installing vie docker ###
* Download project ( git clone git clone https://haanastasia@bitbucket.org/haanastasia/molecular.git )
* Run docker
* docker build -t flexbox .
* docker run -p 30000:30000 flexbox
* open http://localhost:30000

## Built With ##
* SASS - Style sheets
* Bootstrap 4 -  Website layouts

## Authors ##

* Anastasia Dolgopolova (https://bitbucket.org/haanastasia/molecular/)